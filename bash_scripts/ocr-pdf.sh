#!/bin/bash
# Small script to minimize typing when using ocrmypdf to ocr pdfs
# By Thomas Hegghammer, Nov 2022
# Place in /home/$USER/.local/bin as ocr-pdf or something, then alias to your needs
input_filename=$1
stem=$(echo $input_filename | cut -f 1 -d '.')
suffix='_searchable.pdf'
output_filename=$(echo $stem$suffix)
ocrmypdf $input_filename $output_filename
echo "Searchable PDF '$output_filename' created."

