#!/bin/bash
# Script to set vivid colors to a specific theme.
# See https://github.com/sharkdp/vivid for details
# I like Molokai and Lava.
# Place in `~/.local/bin` as `set_vivid` and alias to `sv`.
THEME=$1
PART1='export LS_COLORS="$(vivid generate' 
PART2=$THEME\)\"
NEW="$PART1 $PART2"
OLD=$(grep LS_COLORS /home/thomas/.zshrc)
SEDSTRING=s/$OLD/$NEW/g
sed -i -e "$SEDSTRING" /home/thomas/.zshrc
echo Vivid theme set to \"$THEME\". Restart terminal to use it.
