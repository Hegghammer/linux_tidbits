#!/bin/bash
# Script to get exchange rates from the Norges Bank API (https://app.norges-bank.no/query/index.html#/no/)
# and display it in the terminal
# Thomas Hegghammer, November 2022
# Copy to PATH (e.g. ́~/.local/bin) under short name (e.g. valuta) for added convenience 

RAW1=$(curl -s "https://data.norges-bank.no/api/data/EXR/B.GBP.NOK.SP?format=sdmx-json&lastNObservations=1&locale=no" | jq '.data.dataSets | .[].series."0:0:0:0".observations."0"|.[]') 
RAW2=$(curl -s "https://data.norges-bank.no/api/data/EXR/B.USD.NOK.SP?format=sdmx-json&lastNObservations=1&locale=no" | jq '.data.dataSets | .[].series."0:0:0:0".observations."0"|.[]') 
RAW3=$(curl -s "https://data.norges-bank.no/api/data/EXR/B.EUR.NOK.SP?format=sdmx-json&lastNObservations=1&locale=no" | jq '.data.dataSets | .[].series."0:0:0:0".observations."0"|.[]') 
RAWD=$(curl -s "https://data.norges-bank.no/api/data/EXR/B.EUR.NOK.SP?format=sdmx-json&lastNObservations=1&locale=no" | jq '.data.structure.dimensions.observation | .[] | .values | .[] | .name')
VAL1=$(sed -e 's/^"//' -e 's/"$//' <<<$RAW1)
VAL2=$(sed -e 's/^"//' -e 's/"$//' <<<$RAW2)
VAL3=$(sed -e 's/^"//' -e 's/"$//' <<<$RAW3)
RAWD2=$(sed -e 's/^"//' -e 's/"$//' <<<$RAWD)
DAT=$(date -d $RAWD2 +"%d %b %Y")
echo -e "Siste tall fra Norges bank ($DAT):\nGBP 1 = NOK $VAL1\nUSD 1 = NOK $VAL2\nEUR 1 = NOK $VAL3"
