#/bin/bash
# Script to print out (and copy to clipboard) 
# just the URL from a Wallabag entry 
# retrieved with https://github.com/artur-shaik/wallabag-client
# TH 23 July 2024
URL=$(wallabag info $1 | grep -E 'Url:' | awk '{print $2}')
echo $URL
echo $URL | xclip -selection clipboard

