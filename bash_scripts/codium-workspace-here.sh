#!/bin/bash
# Script to create a `.code-workspace` file in the current directory and 
# open the workspace in VSCodium.
# Presupposes installation of VSCodium that is launched with `codium`.
# Thomas Hegghammer, January 2024
# Copy to PATH (e.g. ́~/.local/bin) for access from everywhere.

DIRNAME=$(pwd)
BASENAME=$(basename $DIRNAME)
FILENAME="${BASENAME}.code-workspace"
cat > $FILENAME<< EOF
{
        "folders": [
                {
                        "path": "."
                }
        ]
}
EOF
codium $FILENAME
