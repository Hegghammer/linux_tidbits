#/bin/bash
# Script to open the URL from a Wallabag entry 
# retrieved with https://github.com/artur-shaik/wallabag-client
# TH 23 July 2024
LINK=$(wallabag info $1 | grep -E 'Url:' | awk '{print $2}')
firefox $LINK

