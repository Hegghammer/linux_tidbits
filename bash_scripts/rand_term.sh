#!/bin/bash
# Script to select a random Kitty theme from a shortlist.
# Place in ~/.local/bin as `rand_term` and alias to `ktr`
themes=("Mayukai" "Alabaster" "Red Alert" "Goa Base" "Adventure Time" "vimbones" "Vibrant Ink" "Ubuntu" "Modus Operandi" "Gruvbox Light" "Hard Grass" "Fun Forrest" "Forest Night" "Cobalt2" "Borland" "Argonaut" "Aquarium Dark")
chosen=$(shuf -n1 -e "${themes[@]}")
kitty +kitten themes $chosen
echo Terminal now running $chosen theme.
exit 0
