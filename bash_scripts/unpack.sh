#!/bin/bash
# Script to decompress the most common archive types so I don't have to look 
# them up anymore.
#
# Rename to "unpack" and place in $PATH for greater convenience.
#
# Thomas Hegghammer August 2022

filename=$1
stem=$(echo $filename | cut -d '.' -f1)
extension=${filename#"$stem"}
case $extension in
  .zip)		echo "Extracting $extension archive .."
  		unzip "$filename" ;;
  .tar) 	echo "Extracting $extension archive .."
  		tar -xvf "$filename" ;;
  .tar.gz) 	echo "Extracting $extension archive .."
  	   	tar -xvzf "$filename" ;;
  .tar.bz2) echo "Extracting $extension archive .."
  	    tar -xvjf "$filename" ;;
  .tar.xz) 	echo "Extracting $extension archive .."
   	   	tar -xvf "$filename" ;;
  *) 		echo "Unrecognized file format. Exiting .." ;;
esac

