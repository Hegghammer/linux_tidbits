#!/bin/bash

: '  
# Small script to simplify pandocing from markdown to pdf
By Thomas Hegghammer, 10 Aug 2022

## Motivation

Almost all my pandoc usage involves conversion from .md to .pdf using a limited set of parameters. Might as well simplify the process.

## Requirements

1)  environment variables $PANDOC_DIR, $PAN, $PAN_EIS, $PAN_CHIC, $PAN_EIS_CHIC set in .profile or .bashrc
    
    For example:
    export PANDOC_DIR="/home/$USER/templates/pandoc"
    export PAN="--resource-path $PANDOC_DIR --bibliography master.bib --citeproc"
    export PAN_CHIC="--resource-path $PANDOC_DIR --bibliography master.bib --csl chicago17_full.csl --citeproc"
    export PAN_EIS="--resource-path $PANDOC_DIR --bibliography master.bib --template eisvogel --citeproc"
    export PAN_EIS_CHIC="--resource-path $PANDOC_DIR --bibliography master.bib --csl chicago17_full.csl --template eisvogel --citeproc"

2) a master.bib file in the resource path ($PANDOC_DIR)

3) a chicago17_full.csl template in the resource path

## Usage

No flag pandocs from md to pdf with reference to my master.bib
-e pandocs from md to pdf with eisvogel template
-c pandocs with citations formated in chicago style

For example:
`./pdoc.sh letter.md`
`./pdoc.sh -e letter.md`
`./pdoc.sh -ce letter.md`

## Recommendations  

1) Rename to "pdoc" and place in $PATH, e.g. /home/$USER/.local/bin, for easy execution from anywhere
2) Create alias, eg "p" for "pdoc", "pe" for "pdoc -e", etc, for even less typing

### TODO: Add more error handling and help flag
'  

e_flag=false
c_flag=false

while getopts "ec" opt; do
  case $opt in
    e) e_flag=true ;;
    c) c_flag=true ;;
    :) echo "missing argument for option -$OPTARG"; exit 1;;
    \?) echo "unknown option -$OPTARG"
  esac
done

shift $(( OPTIND - 1 ))

input_filename=$1
stem=$(echo $input_filename | cut -f 1 -d '.')
output_filename=$(echo $stem.pdf)

if $e_flag && $c_flag; then
    pandoc $(echo $PAN_EIS_CHIC) $input_filename -o $output_filename
elif $e_flag; then
    pandoc $(echo $PAN_EIS) $input_filename -o $output_filename
elif $c_flag; then
    pandoc $(echo $PAN_CHIC) $input_filename -o $output_filename
else  
    pandoc $(echo $PAN) $input_filename -o $output_filename
fi

echo "File '$input_filename' successfully converted to '$output_filename'."

