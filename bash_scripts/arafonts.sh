#!/bin/bash
# Script to display all the fonts on the system that can have Arabic letters
# Useful for determining which font you can use in Latex, in expressions like
# \babelfont[arabic]{rm}[Scale=1.7]{<FONTNAME>}
#
# Save as arafonts (or something) and place in $PATH for more convenience
#
# Thomas Hegghammer August 2022

fc-list :lang=ar |
awk -F ':' '{print $2}' |
while read -r line; do
if [[ $line =~ "," ]];
then echo $line | awk -F ',' '{print $2}';
else echo $line;
fi;
done |
awk '!a[$0]++' |
sort
