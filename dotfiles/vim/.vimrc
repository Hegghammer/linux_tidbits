" VIMRC
" Thomas Hegghammer
" I use the filetype plugin, so there are additional,
" filetype-specific settings in ~/.vim/ftplugin/

" PLUGINS
call plug#begin()
"Plug 'preservim/vim-markdown'
Plug 'tpope/vim-sensible'
Plug 'SirVer/ultisnips'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'preservim/vim-colors-pencil'
Plug 'gerardbm/vim-atomic'
Plug 'tpope/vim-commentary'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhinz/vim-startify'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }                                                                                        
Plug 'junegunn/fzf.vim'
Plug 'msprev/fzf-bibtex'
Plug 'preservim/nerdtree'
Plug 'romainl/vim-cool'
Plug 'ayu-theme/ayu-vim'
Plug 'conornewton/vim-pandoc-markdown-preview'
Plug 'shime/vim-livedown' 
Plug 'vimwiki/vimwiki'
call plug#end()               

" Vim Wiki
let g:vimwiki_list = [{'path': '~/heggcloud/Documents/shared/vimwiki/', 'syntax': 'markdown'}]
au FileType vimwiki setlocal shiftwidth=6 tabstop=6 noexpandtab

" ======= SYSTEM
set ma
set mouse=a
set ttymouse=sgr
set clipboard=unnamedplus
set viminfofile=~/.vim/.viminfo
set encoding=utf-8
set arabicshape
set nocompatible
set nobackup
set incsearch
set ignorecase
set smartcase
set showmatch
set hlsearch
set history=20
set undodir=~/.vim/undodir
set undofile
set undoreload=10000
filetype on
filetype plugin on
let NERDTreeIgnore=['\.git$', '\.jpg$', '\.mp4$', '\.ogg$', '\.iso$', '\.pdf$', '\.pyc$', '\.odt$', '\.png$', '\.gif$', '\.db$']
:cnoremap YMD <C-R>=strftime("%Y-%m-%d")<CR>

" ====== AESTHETICS
let &fillchars ..= ',eob: ' "no tildes 
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu
"colorscheme pencil
"colorscheme atomic
set termguicolors
set nonumber
set cursorline
set shiftwidth=4
set tabstop=4
set expandtab
set nowrap
set linebreak
set formatoptions-=t
set virtualedit=all
syntax on
"let g:vim_markdown_folding_disabled = 1
"let g:vim_markdown_conceal = 0
let g:limelight_conceal_ctermfg = 'Grey'
let g:limelight_default_coefficient = 0.6
autocmd! User GoyoEnter Limelight " Have Limelight appear only with Goyo
autocmd! User GoyoLeave Limelight!
let &t_SI = "\e[5 q" "blinking beam cursor in insert mode
let &t_SR = "\e[3 q" "blinking block in replace mode
let &t_EI = "\e[2 q" "solid block in normal mode

" ====== KEYMAPPINGS
let mapleader = '\'
nnoremap j gj
nnoremap k gk
nnoremap <silent> 0 g0
"nnoremap <silent> $ g$
nnoremap <silent> $ g_
nnoremap  <silent> <Up>      gk
inoremap <silent> <Up>      <C-o>gk
nnoremap  <silent> <Down>    gj
inoremap <silent> <Down>    <C-o>gj
nnoremap  <silent> <home>    g<home>
inoremap <silent> <home>    <C-o>g<home>
"nnoremap  <silent> <End>     g<End>
nnoremap  <silent> <End>     g_
"inoremap <silent> <End>     <C-o>g<End>
inoremap <silent> <End>     <C-o>g_
inoremap jj <Esc>
nnoremap <space> :
nnoremap n nzz
nnoremap N Nzz
nnoremap Y y$
"remove empty lines:
nnoremap <Leader>l :g/^$/d<CR>
"substitute multiple spaces with single:
nnoremap <Leader>s :%s# \+# #g<CR>
nnoremap <C-S> :update<cr>
inoremap <C-S> <Esc>:update<cr>gi
nnoremap zz :update<cr>
nnoremap sss :sav  
inoremap sss <Esc>:sav 
noremap <C-e> <Esc>A
noremap <C-a> <Esc>ggVG
inoremap <C-a> <Esc>ggVG
imap ZZ <Esc>ZZ       
imap ZQ <Esc>ZQ
"move to link (open with gx)
nnoremap <C-l> /http<CR>


nmap <M-up> dd2kp
nmap <M-down> ddp

fu! ToggleCurline ()
  if &cursorline && &cursorcolumn
    set nocursorline
    set nocursorcolumn
  else
    set cursorline
    set cursorcolumn
  endif
endfunction

noremap <F1> :Goyo<CR>
map <silent><F2> :call ToggleCurline()<CR>
nnoremap <F3> :set nonumber!<CR>
nnoremap <F4> :NERDTreeToggle<CR>
nmap <F5> :so ~/.vimrc<CR>
nnoremap <F6> :UltiSnipsEdit<CR>
nnoremap <Leader>r :call UltiSnips#RefreshSnippets()<CR>
" map <F7> :-1 pu=strftime('%Y-%m-%d_%A')<CR>
nmap <F7> :w !sudo tee % <CR>
imap <F7> <ESC>:w !sudo tee % <CR>
map <F8> :exe 'sav ~/temp/note'.strftime("%Y-%m-%d_%H%M").'.md'
nnoremap <F9> :set background=dark <CR>
nnoremap <F10> :set background=light <CR>
"F11 toggle fullscreen
nmap <F12> :w<CR>:LivedownToggle<CR>
imap <F12> <ESC>:w<CR>:LivedownToggle<CR>a
" View at http://localhost:1337 

nnoremap <Leader>e :<C-U>call EngType()<CR>
nnoremap <Leader>a :<C-U>call AraType()<CR>
" ====== ARABIC
" Switch to English - function
function! EngType()
" To switch back from Arabic
  set keymap=us-altlatin
"  set keymap= " Restore default (US) keyboard layout
  set norightleft
endfunction
" Switch to Arabic - function
function! AraType()
    set keymap=arabic-pc "Modified keymap. File in ~/.vim/keymap/
    set rightleft
    set nospell
endfunction
" From Andreas - Med markören på ett arabisk ord i en markdown-fil i vim, gr i normal
" mode för att markera det som arabisk text enligt ovan:
autocmd FileType markdown,markdown.pandoc,mail nnoremap <buffer>gr lmfbi[<esc>ea]{lang=ar}<esc>`f

" Easier Arabic transcription
"{{{2 EALL transcription
function! EALLToggle()
   if !exists("b:eallmappings")
     let b:eallmappings = 0
   endif
   if b:eallmappings == 0
     let b:eallmappings = 1
     echo "EALL mappings on for this buffer"
     inoremap <buffer> aa ā
     inoremap <buffer> ii ī
     inoremap <buffer> uu ū
     inoremap <buffer> AA Ā
     inoremap <buffer> II Ī
     inoremap <buffer> UU Ū
     inoremap <buffer> .d ḍ
     inoremap <buffer> .D Ḍ
     inoremap <buffer> .t ṭ
     inoremap <buffer> .T Ṭ
     inoremap <buffer> .s ṣ
     inoremap <buffer> .S Ṣ
     inoremap <buffer> .r ṛ
     inoremap <buffer> .R Ṛ
     inoremap <buffer> .z ẓ
     inoremap <buffer> .Z Ẓ
     inoremap <buffer> .h ḥ
     inoremap <buffer> .H Ḥ
     inoremap <buffer> .g ġ
     inoremap <buffer> .G Ġ
     inoremap <buffer> vs š
     inoremap <buffer> vS Š
     inoremap <buffer> _d ḏ
     inoremap <buffer> _D Ḏ
     inoremap <buffer> _t ṯ
     inoremap <buffer> _T Ṯ
   elseif b:eallmappings == 1
     let b:eallmappings = 0
     echo "EALL mappings off"
     iunmap <buffer>aa
     iunmap <buffer>ii
     iunmap <buffer>uu
     iunmap <buffer>AA
     iunmap <buffer>II
     iunmap <buffer>UU
     iunmap <buffer>.d
     iunmap <buffer>.D
     iunmap <buffer>.t
     iunmap <buffer>.T
     iunmap <buffer>.s
     iunmap <buffer>.S
     iunmap <buffer>.z
     iunmap <buffer>.Z
     iunmap <buffer>.h
     iunmap <buffer>.H
     iunmap <buffer>.g
     iunmap <buffer>.G
     iunmap <buffer>vs
     iunmap <buffer>vS
     iunmap <buffer>_d
     iunmap <buffer>_D
     iunmap <buffer>_t
     iunmap <buffer>_T
   endif
endfunction

"command! EALLToggle call EALLToggle()  

nnoremap <Leader>t :<C-U>call EALLToggle()<CR> 

" ===== Markdown PDF preview
nnoremap <leader>p :StartMdPreview<CR>
"nnoremap <leader>å :StopMdPreview<CR>
" default pdf viewer for pandoc preview
let g:md_pdf_viewer="zathura"
" Default pandoc arguments for PDF preview
"let g:md_args = '--resource-path $PANDOC_DIR --bibliography master.bib --csl chicago17_full.csl --citeproc --listings --template eisvogel -V disable-header-and-footer:true'

" ====== SNIPS
let g:UltiSnipsSnippetDirectories = ['~/.vim/ultisnips', 'ultisnips']
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsEditSplit="vertical"

" ====== STATUS LINE
"let g:airline_theme='light'
"let g:airline_theme='transparent2'
let g:airline_section_a = '%{hostname()}'
let g:airline_section_z = airline#section#create('%#__accent_bold#%4l%#__restore__#%#__accent_bold#/%L%#__restore__# %3v')
let g:airline_section_y = airline#section#create('')
let g:airline#extensions#whitespace#enabled = 0

" ========== FZF BIBTEX
" https://github.com/msprev/fzf-bibtex

function! s:bibtex_cite_sink(lines)
    let r=system("bibtex-cite ", a:lines)
    execute ':normal! a' . r
endfunction

function! s:bibtex_markdown_sink(lines)
    let r=system("bibtex-markdown ", a:lines)
    execute ':normal! a' . r
endfunction

nnoremap <silent> <leader>c :call fzf#run({
                        \ 'source': 'bibtex-ls',
                        \ 'sink*': function('<sid>bibtex_cite_sink'),
                        \ 'down': '50%',
                        "\ 'options': '--ansi --layout=reverse-list --multi --prompt "Cite> "'})<CR>
                        \ 'options': '--ansi --reverse --multi --prompt "Cite> "'})<CR>

nnoremap <silent> <leader>m :call fzf#run({
                        \ 'source': 'bibtex-ls',
                        \ 'sink*': function('<sid>bibtex_markdown_sink'),
                        \ 'down': '50%',
                        "\ 'options': '--ansi --layout=reverse-list --multi --prompt "Markdown> "'})<CR>
                        \ 'options': '--ansi --reverse --multi --prompt "Markdown> "'})<CR>

" ========= STARTIFY
let g:startify_bookmarks = [ '~/.vimrc', '~/.bash_aliases' ]

