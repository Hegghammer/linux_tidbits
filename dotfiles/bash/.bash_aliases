# ===============
# Minimal .bash_aliases, for servers
#
# Thomas Hegghammer
#
# Prerequisites: 
# sudo apt install -y htop mlocate ncdu tree ufw unzip xclip zip
# For ubuntu 20.10 and above: sudo apt install -y exa
# ===============

alias c='clear'
alias rf=". /home/$USER/.bashrc"
alias e='exit'
alias rb='reboot'
alias sd='sudo shutdown now'
alias s='sudo -i'
alias l='exa -l'
alias ll='exa -alFh'
alias lll='exa -abghHliS'
alias tr='exa --tree --level=2'
alias treee='exa --tree --level=2 --long'
alias lc='locate'
alias d='ncdu'
alias df='df -h'
alias du='du -h'
alias h='htop'
alias up='sudo apt update && sudo apt -y full-upgrade'
alias ar='sudo apt -y autoremove'
alias in='sudo apt -y install'
alias purge='sudo apt --purge remove'
alias untar='tar -xvzf'
alias indeb='sudo dpkg -i'
alias fw='sudo ufw status numbered'
alias myip='curl https://ipecho.net/plain ; echo'
alias ping='ping -c 4'
alias sshconf='sudo vi /etc/ssh/sshd_config'
alias edal='vi /home/$USER/.bash_aliases'
alias edrc='vi /home/$USER/.bashrc'
alias edpf='vi /home/$USER/.bash_profile'
function fp () { readlink -f $1 | xclip; } 
alias cpy='xclip -selection clipboard'
alias pc='pwd | cpy'
alias v='vim'
alias gs='git status'
alias gl='git log --oneline'
alias gc='git checkout'
alias ga='git add'
